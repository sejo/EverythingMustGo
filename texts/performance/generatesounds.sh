#!/bin/bash
echo "generating wav files..."
text2wave -o intro.wav -F 44100 moderator_intro.txt
text2wave -o otherqa.wav -F 44100 moderator_otherquestion.txt
text2wave -o qa.wav -F 44100 moderator_qa.txt
text2wave -o outro.wav -F 44100 moderator_outro.txt
text2wave -o cyborg.wav -F 44100 cyborg.txt
text2wave -o silence.wav -F 44100 silence.txt
text2wave -o prerecordedlaughter.wav -F 44100 laughter.txt

echo "copying wav files to sound folder..."
cp -v *wav ../../sound/performance/
echo "done!"
