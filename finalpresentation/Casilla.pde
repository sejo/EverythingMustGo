class Casilla{
	boolean visited;
	PImage img;
	PImage thumb;
	PVector p;
	float w,h;
	float ratio;

	Casilla(int i, float x, float y){
		img = loadImage(String.format("slides/slide-%02d.png",i));
		thumb = loadImage(String.format("slides/thumbnails/slide-%02d.png",i));

		p = new PVector(x,y);
		w = 40;
		h = 30;

	}

	void draw(boolean isCurrent){
		image(thumb,p.x,p.y);
		if(visited){
			fill(0,20);
			stroke(20);
			strokeWeight(2);
		}
		else{
			noFill();
			stroke(255);
			if(isCurrent){
				strokeWeight(5);
			}
			else{
				strokeWeight(3);
			}
		}

		rect(p.x,p.y,w,h);


	}

	PImage getImg(){
		return img;
	}

	PVector getPos(){
		return p;
	}

	void reset(){
		visited = false;
	}

	void doVisit(){
		visited = true;
	}


}
