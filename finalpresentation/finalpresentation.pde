int N_CASILLAS = 41;

Casilla[] casillas;
boolean[] indexTaken;

PVector cpos;
int current;
int next;
boolean doMove;
int moveCount;
float time;

int TIMER_SEC = 20;
boolean enableTimer;
int timer;
int alphaFg;

void setup(){
	fullScreen();
//	size(1024,768);
	casillas = new Casilla[N_CASILLAS];

	indexTaken = new boolean[N_CASILLAS];
	indexTaken[0] = true; // fixed first blank slide
	for(int i=1; i<N_CASILLAS; i++){
		indexTaken[i] = false;
	}


	float a,r,x,y,cx,cy;
	r = height*0.45;
	cx = width/2;
	cy = height/2;
	a = 0;
	int index = 0;
	for(int i=0; i<N_CASILLAS; i++){
		x = r*cos(a)+cx;
		y = r*sin(a)+cy;

		if( i != 0){
			do{
				index = int(random(N_CASILLAS));
			}while( indexTaken[index] );
			indexTaken[index] = true;
		}

		casillas[i] = new Casilla(index,x,y);
		a += TWO_PI/N_CASILLAS;
	}

	current = 0;
	cpos = new PVector();
	cpos.set(casillas[current].getPos());

}

void draw(){
	background(0);
	Casilla c = casillas[current];

	rectMode(CENTER);
	imageMode(CENTER);
	PImage ci = c.getImg();
	image(ci,width/2,height/2,height*ci.width/ci.height,height);
	for(int i=0; i<N_CASILLAS; i++){
		casillas[i].draw(i==current);
	}

	fill(0,255,0);
	ellipse(cpos.x,cpos.y,42,42);

	if(moveCount > 0){
		if(time < 1){
			time += 0.2;
			cpos.lerp(casillas[next].p,time);
		}
		else{
			current = availableNext(current);
			newNext();
			time = 0;
			moveCount--;
		}
		
	}

	if(enableTimer){
		if(millis() - timer > TIMER_SEC*1000){
			alphaFg = 255;	
			enableTimer = false;
		}
	}

	noStroke();
	fill(255,alphaFg);
	rect(0,0,width*2,height*2);
	if(alphaFg>0){
		alphaFg -=10;
	}

}

void keyPressed(){
	if((key>='1' && key<='9') || (key>='a'&& key<='c')){
		casillas[current].doVisit();
		time = 0;
		if(key<='9'){
			moveCount = key-'0';
		}
		else{ // a to c
			moveCount = key-'a'+10;
		}
		newNext();
	}
	else if(key=='0'){
		moveCount = int(random(1,13));
		casillas[current].doVisit();
		time = 0;
		newNext();
	}
	else if(key=='r'){
		reset();
	}
	else if(key=='t'){ // start timer
		enableTimer = true;
		timer = millis();
	}

}


void newNext(){
	next = availableNext(current);
}

int availableNext(int n){
	do{
		n = (n+1)%N_CASILLAS;
	}while(casillas[n].visited);
	return n;
}

void reset(){
	for(int i=0; i<N_CASILLAS; i++){
		casillas[i].reset();
	}
	current = 0;
	cpos = new PVector();
	cpos.set(casillas[current].getPos());

}

