#!/bin/bash
# convert lines of text to individual audiofiles
# run ./createLinesAudios.sh textfile outputdir/
filename=$1
directory=$2
i=0
mkdir -p $directory
while read line; do
	printf -v num $directory"%03d.wav" $i;
	echo $num $line;
	echo $line | text2wave -o $num -F 44100
	let "i++"
done < $filename
