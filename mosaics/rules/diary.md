# diary rules
* one entry of at least 100 words per day
* the list/grid of seeds can't be changed while a diary is in process

## process to start a new diary
* use 6 coins to decide how many days the current diary will include, from 1 to 64
* define the list/grid of 64 seeds (words, phrases) for the current diary
* create a header for the diary: number of days, date range, list/grid of seeds
