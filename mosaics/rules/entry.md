# entry rules
* entries 

## process to generate an entry
* while the total of words is less than one hundred, generate a line according to the following process


## process to generate a line for an entry
* use 6 coins to decide which seed to use from the list/grid
* use 6 coins to decide how many words the current line will have, from 1 to 64
* use 1 coin to decide the language that the line will be written in 
* write the amount of words inspired by the seed, in the corresponding language
* use 2 coins to decide the format of the line: normal, bold, italics or strikethrough.
