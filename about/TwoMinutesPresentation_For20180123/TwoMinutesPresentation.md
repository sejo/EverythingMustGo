everything must go

my project is a poetic exploration in how to let go the ideology that is leading us to collective suicide and ecocide in the following years.

i have decided to follow a process of writing text mosaics, and of sharing, reading and performing(?) them, 
guided by chance operations a la john cage, individually and in groups.

in my research about the collapse of civilization and the living world,
i have realized that there is already a lot of information, resources, motivated people and compelling stories.

we know that we shouldn't be flying airplanes or driving cars, creating, consuming and disposing this much trash, using and extracting this amount of fossil fuels, eating animal-based products, et cetera.
however, there's a difficulty in letting go, and a disagreement of how much we actually need to change.

it appears to be a problem of denial, of massive distraction, of alienation, of imagination, of language.

instead of being yet another male voice yelling and/or telling others what to do
i want to write, share, read, perform these texts by myself and with whoever is interested, exploring how to radically let go.

for example, i wonder
how to let go of expectations, of aversion to doubt, of aversion to boredom
how to let go the need for control, convenience, immediacy, logic, selfishness, order, isolation, speed, comfort, productivity, status, virtuosity, innovation, goals, spectacle, class, achievements, etiquette, competition, exploitation, results, fear, consumerism, money, war, power, masculinity, accumulation, proficiency and growth
how to let go the need of being convincing, knowledgeable, always right, superior, entertaining, strong, understood, coherent, objective, successful, loud, intellectual, interesting, wise, grammatically correct

and finally, i wonder how to turn the tables in order to find balance, restoration, healing, social and environmental justice, sharing, peace, connection, compassion, and happiness for all.
