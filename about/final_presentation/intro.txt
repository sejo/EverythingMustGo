hello, i'm sejo vega-cebrián. 
i come from mexico city and i will share with you all my thesis project called
everything must go, or a journey to obtain an all-access pass to existential anxiety and attain some kind of enlightenment.

the project is a performance with a companion booklet
before i talk about it, because when i talk about it i start messing up, i want to show you a trailer i made after the second (out of three) showing of the event.

[trailer]

the event is an immersive experience that happens in a dark room and lasts around one hour.

it is divided in two parts; in the first one the conversation is reserved for the three participants plus the random remarks of the moderator, and in the second part the audience is repeatedly invited to contribute with questions.
the durations and many other factors are determined by chance live during each event.

as part of the experience, i made a companion booklet for consultation. it contains a comprehensive collection of the scripts, ideas, attempted descriptions of thw work, and other fragments of text surrounding the genesis of the experience.

i started this process very overwhelmed by the climate breakdown and other social crises that are apparently leading us to collapse.

there are many things to say about this journey, and i have been always unsuccessful in trying to communicate all of them.
so, being consistent with my chance-based project, the following section of the presentation will be a random and probably incomplete selection of slides from a vast collection i made.

please keep in mind that i would say that the form, or medium, i'll be using, is part of the message i'm trying to convey.
i made a program for this, so we can say it's also part of the thesis.

in any case, i would say, that if you take away something from my thesis project, it should be the following:

1) sejo is in a state of disbelief about human order, control, civilization.
2) sejo thinks that using chance operations is a possible way of letting go of the illusion of control.
3) sejo is full of... contradictions, and he's embracing and sharing them. starting from the fact that he is using the english language, he's explaining all this to a human audience, he created a program for these chance operations, and he is hopefully getting a master's degree with this project.

