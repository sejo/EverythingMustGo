#!/bin/bash
# generate slides for finalpresentation sketch
d=$(pwd)
"deleting previous slides..."
cd ../../finalpresentation/slides
rm -v *png thumbnails/*png
cd $d
echo "converting pdf..."
convert -density 90 slides.pdf -quality 90 -colorspace RGB ../../finalpresentation/slides/slide-%02d.png
echo "making thumbnails..."
cd ../../finalpresentation/slides
for f in *png; do convert -verbose $f -resize 40x40 thumbnails/$f; done
cd $d
echo "finished!"
