command used to convert pdf to inverted images with transparent background:
`
convert -verbose -density 300 emptywords_part4.pdf -channel RGB -negate -transparent black pages/page.png
`
