Image processing: contrast stretching and negation:
`for f in *jpg; do convert -verbose $f -contrast-stretch 0 -negate coninv/$f`

Go to processed directory
`cd coninv`

Converting to pdf:
`for f in *jpg; do convert $f ${f%jpg}pdf; done`

Concatenating pdf:
`pdftk *pdf 20180121_195906.pdf cat output ../../TwoMinutesPresentation.pdf`
