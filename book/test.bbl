% $ biblatex auxiliary file $
% $ biblatex bbl format version 2.9 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\datalist[entry]{nty/global//global/global}
  \entry{bifo}{book}{}
    \name{author}{1}{}{%
      {{hash=BF}{%
         family={Berardi},
         familyi={B\bibinitperiod},
         given={Franco},
         giveni={F\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Semiotext(e)}%
    }
    \strng{namehash}{BF1}
    \strng{fullhash}{BF1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{The Uprising: On Poetry and Finance}
    \field{year}{2012}
  \endentry

  \entry{theatreoftheoppressed}{book}{}
    \name{author}{1}{}{%
      {{hash=BA}{%
         family={Boal},
         familyi={B\bibinitperiod},
         given={Augusto},
         giveni={A\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Editorial Nueva Imagen}%
    }
    \strng{namehash}{BA1}
    \strng{fullhash}{BA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{Teatro del Oprimido/1 Teoría y Práctica}
    \field{year}{1980}
  \endentry

  \entry{posthuman}{book}{}
    \name{author}{1}{}{%
      {{hash=BR}{%
         family={Braidotti},
         familyi={B\bibinitperiod},
         given={Rosi},
         giveni={R\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Polity Press}%
    }
    \strng{namehash}{BR1}
    \strng{fullhash}{BR1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{The Posthuman}
    \field{year}{2013}
  \endentry

  \entry{ecodeviance}{book}{}
    \name{author}{1}{}{%
      {{hash=C}{%
         family={CAConrad},
         familyi={C\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Wave Books}%
    }
    \strng{namehash}{C1}
    \strng{fullhash}{C1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{C}
    \field{sortinithash}{C}
    \field{title}{Ecodeviance (soma)tics for the future wilderness}
    \field{year}{2014}
  \endentry

  \entry{yearfrommonday}{book}{}
    \name{author}{1}{}{%
      {{hash=CJ}{%
         family={Cage},
         familyi={C\bibinitperiod},
         given={John},
         giveni={J\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Wesleyan University Press}%
    }
    \strng{namehash}{CJ1}
    \strng{fullhash}{CJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{C}
    \field{sortinithash}{C}
    \field{title}{A Year From Monday}
    \field{year}{1967}
  \endentry

  \entry{silence}{book}{}
    \name{author}{1}{}{%
      {{hash=CJ}{%
         family={Cage},
         familyi={C\bibinitperiod},
         given={John},
         giveni={J\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Wesleyan University Press}%
    }
    \strng{namehash}{CJ1}
    \strng{fullhash}{CJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{C}
    \field{sortinithash}{C}
    \field{title}{Silence: 50th anniversary edition}
    \field{year}{2011}
  \endentry

  \entry{riane}{book}{}
    \name{author}{1}{}{%
      {{hash=ER}{%
         family={Eisler},
         familyi={E\bibinitperiod},
         given={Riane},
         giveni={R\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Berret-Koehler Publishers}%
    }
    \strng{namehash}{ER1}
    \strng{fullhash}{ER1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{E}
    \field{sortinithash}{E}
    \field{title}{The Real Wealth of Nations}
    \field{year}{2007}
  \endentry

  \entry{momo}{book}{}
    \name{author}{1}{}{%
      {{hash=EM}{%
         family={Ende},
         familyi={E\bibinitperiod},
         given={Michael},
         giveni={M\bibinitperiod},
      }}%
    }
    \list{publisher}{2}{%
      {Doubleday}%
      {Company, Inc.}%
    }
    \strng{namehash}{EM1}
    \strng{fullhash}{EM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{E}
    \field{sortinithash}{E}
    \field{title}{Momo}
    \field{year}{1984}
  \endentry

  \entry{theatreoftheabsurd}{book}{}
    \name{author}{1}{}{%
      {{hash=EM}{%
         family={Esslin},
         familyi={E\bibinitperiod},
         given={Martin},
         giveni={M\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Vintage Books}%
    }
    \strng{namehash}{EM2}
    \strng{fullhash}{EM2}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{E}
    \field{sortinithash}{E}
    \field{edition}{3}
    \field{title}{The Theatre of the Absurd}
    \field{year}{2004}
  \endentry

  \entry{taoofpooh}{book}{}
    \name{author}{1}{}{%
      {{hash=HB}{%
         family={Hoff},
         familyi={H\bibinitperiod},
         given={Benjamin},
         giveni={B\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {E.P. Dutton, Inc.}%
    }
    \strng{namehash}{HB1}
    \strng{fullhash}{HB1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{H}
    \field{sortinithash}{H}
    \field{title}{The Tao of Pooh}
    \field{year}{1982}
  \endentry

  \entry{cageecopoetics}{book}{}
    \name{author}{1}{}{%
      {{hash=JP}{%
         family={Jaeger},
         familyi={J\bibinitperiod},
         given={Peter},
         giveni={P\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Bloomsbury}%
    }
    \strng{namehash}{JP1}
    \strng{fullhash}{JP1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{J}
    \field{sortinithash}{J}
    \field{title}{John Cage and Buddhist Ecopoetics}
    \field{year}{2013}
  \endentry

  \entry{darkmountain}{book}{}
    \name{author}{2}{}{%
      {{hash=KP}{%
         family={Kingsnorth},
         familyi={K\bibinitperiod},
         given={Paul},
         giveni={P\bibinitperiod},
      }}%
      {{hash=HD}{%
         family={Hine},
         familyi={H\bibinitperiod},
         given={Dougald},
         giveni={D\bibinitperiod},
      }}%
    }
    \strng{namehash}{KPHD1}
    \strng{fullhash}{KPHD1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{K}
    \field{sortinithash}{K}
    \field{title}{Uncivilisation: The Dark Mountain Manifesto}
    \field{year}{2009}
  \endentry

  \entry{patterning}{book}{}
    \name{author}{1}{}{%
      {{hash=LJ}{%
         family={Lent},
         familyi={L\bibinitperiod},
         given={Jeremy},
         giveni={J\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Prometheus Books}%
    }
    \strng{namehash}{LJ1}
    \strng{fullhash}{LJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{L}
    \field{sortinithash}{L}
    \field{title}{The Patterning Instinct}
    \field{year}{2017}
  \endentry

  \entry{mcluhan}{book}{}
    \name{author}{2}{}{%
      {{hash=MM}{%
         family={McLuhan},
         familyi={M\bibinitperiod},
         given={Marshall},
         giveni={M\bibinitperiod},
      }}%
      {{hash=FQ}{%
         family={Fiore},
         familyi={F\bibinitperiod},
         given={Quentin},
         giveni={Q\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Bantam Books, Inc.}%
    }
    \strng{namehash}{MMFQ1}
    \strng{fullhash}{MMFQ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{M}
    \field{sortinithash}{M}
    \field{title}{The Medium is the Massage: An Inventory of Effects}
    \field{year}{1967}
  \endentry

  \entry{mess}{book}{}
    \name{author}{1}{}{%
      {{hash=MG}{%
         family={Monbiot},
         familyi={M\bibinitperiod},
         given={George},
         giveni={G\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Verso Books}%
    }
    \strng{namehash}{MG1}
    \strng{fullhash}{MG1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{M}
    \field{sortinithash}{M}
    \field{title}{How did we get into this mess?}
    \field{year}{2016}
  \endentry

  \entry{outofthewreckage}{book}{}
    \name{author}{1}{}{%
      {{hash=MG}{%
         family={Monbiot},
         familyi={M\bibinitperiod},
         given={George},
         giveni={G\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Verso Books}%
    }
    \strng{namehash}{MG1}
    \strng{fullhash}{MG1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{M}
    \field{sortinithash}{M}
    \field{title}{Out of the Wreckage: A New Politics for an Age of Crisis}
    \field{year}{2017}
  \endentry

  \entry{humankind}{book}{}
    \name{author}{1}{}{%
      {{hash=MT}{%
         family={Morton},
         familyi={M\bibinitperiod},
         given={Timothy},
         giveni={T\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Verso}%
    }
    \strng{namehash}{MT1}
    \strng{fullhash}{MT1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{M}
    \field{sortinithash}{M}
    \field{title}{Humankind - Solidarity with nonhuman people}
    \field{year}{2017}
  \endentry

  \entry{geographyofthought}{book}{}
    \name{author}{1}{}{%
      {{hash=NR}{%
         family={Nisbett},
         familyi={N\bibinitperiod},
         given={Richard},
         giveni={R\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {The Free Press}%
    }
    \strng{namehash}{NR1}
    \strng{fullhash}{NR1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{N}
    \field{sortinithash}{N}
    \field{title}{The Geography of Thought}
    \field{year}{2003}
  \endentry

  \entry{anabella}{book}{}
    \name{author}{1}{}{%
      {{hash=PA}{%
         family={Pareja},
         familyi={P\bibinitperiod},
         given={Anabella},
         giveni={A\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Centro de Cultura Digital}%
    }
    \strng{namehash}{PA1}
    \strng{fullhash}{PA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{P}
    \field{sortinithash}{P}
    \field{title}{Mientras pienso en otra cosa: Apuntes coreográficos en
  extinción}
    \field{year}{2017}
  \endentry

  \entry{antipoems}{book}{}
    \name{author}{1}{}{%
      {{hash=PN}{%
         family={Parra},
         familyi={P\bibinitperiod},
         given={Nicanor},
         giveni={N\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {New Directions Books}%
    }
    \strng{namehash}{PN1}
    \strng{fullhash}{PN1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{P}
    \field{sortinithash}{P}
    \field{title}{Antipoems: How to look better and feel great /
  antitranslation by Liz Werner}
    \field{year}{2004}
  \endentry

  \entry{elqui2}{book}{}
    \name{author}{1}{}{%
      {{hash=PN}{%
         family={Parra},
         familyi={P\bibinitperiod},
         given={Nicanor},
         giveni={N\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Ediciones Ganymides}%
    }
    \strng{namehash}{PN1}
    \strng{fullhash}{PN1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{P}
    \field{sortinithash}{P}
    \field{title}{Nuevos Sermones y Predicas del Cristo de Elqui}
    \field{year}{1979}
  \endentry

  \entry{elqui1}{book}{}
    \name{author}{1}{}{%
      {{hash=PN}{%
         family={Parra},
         familyi={P\bibinitperiod},
         given={Nicanor},
         giveni={N\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Ediciones Ganymides}%
    }
    \strng{namehash}{PN1}
    \strng{fullhash}{PN1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{P}
    \field{sortinithash}{P}
    \field{edition}{2}
    \field{title}{Sermones y Predicas del Cristo de Elqui}
    \field{year}{1979}
  \endentry

  \entry{essays}{book}{}
    \name{author}{1}{}{%
      {{hash=SW}{%
         family={Shawn},
         familyi={S\bibinitperiod},
         given={Wallace},
         giveni={W\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Haymarket Books}%
    }
    \strng{namehash}{SW1}
    \strng{fullhash}{SW1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{Essays}
    \field{year}{2009}
  \endentry

  \entry{brecht}{book}{}
    \name{author}{1}{}{%
      {{hash=SA}{%
         family={Squiers},
         familyi={S\bibinitperiod},
         given={Anthony},
         giveni={A\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Rodopi}%
    }
    \strng{namehash}{SA1}
    \strng{fullhash}{SA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{An Introduction to the Social and Political Philosophy of
  Bertolt Brecht}
    \field{year}{2014}
  \endentry

  \entry{introzen}{book}{}
    \name{author}{1}{}{%
      {{hash=SDT}{%
         family={Suzuki},
         familyi={S\bibinitperiod},
         given={Daisetz\bibnamedelima Teitaro},
         giveni={D\bibinitperiod\bibinitdelim T\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Rider \& Company}%
    }
    \strng{namehash}{SDT1}
    \strng{fullhash}{SDT1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{An Introduction to Zen Buddhism}
    \field{year}{1949}
  \endentry

  \entry{zen}{book}{}
    \name{author}{1}{}{%
      {{hash=SDT}{%
         family={Suzuki},
         familyi={S\bibinitperiod},
         given={Daisetz\bibnamedelima Teitaro},
         giveni={D\bibinitperiod\bibinitdelim T\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Image Books Doubleday}%
    }
    \strng{namehash}{SDT1}
    \strng{fullhash}{SDT1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{Zen Buddhism}
    \field{year}{1996}
  \endentry

  \entry{zenmind}{book}{}
    \name{author}{1}{}{%
      {{hash=SS}{%
         family={Suzuki},
         familyi={S\bibinitperiod},
         given={Shunryu},
         giveni={S\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Weatherhill}%
    }
    \strng{namehash}{SS1}
    \strng{fullhash}{SS1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{Zen Mind, Beginner's Mind}
    \field{year}{1999}
  \endentry
\enddatalist
\endinput
