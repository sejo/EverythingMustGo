#!/bin/bash
sed -i 's/<b>/\\textbf{/g' diary.tex
sed -i 's/<\/b>/\}/g' diary.tex
sed -i 's/<i>/\\textit{/g' diary.tex
sed -i 's/<\/i>/}/g' diary.tex
sed -i 's/<del>/\\sout{/g' diary.tex
sed -i 's/<\/del>/}/g' diary.tex
