\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{contents}{iii}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{introduction}{1}
\defcounter {refsection}{0}\relax 
\contentsline {part}{performance texts}{3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{warning}{5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{the moderator: intro}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{diary 1 - january 22 to march 22, 2018}{9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{seed words for diary 1}{29}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{free flow dialogue}{31}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{circular dialogue with chance-based structure}{35}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{both dialogues combined and shuffled together}{41}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{the moderator: questions and answers}{49}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{the moderator: new question}{51}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{the moderator: outro}{53}
\defcounter {refsection}{0}\relax 
\contentsline {part}{meta texts}{55}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{post-first showing thoughts}{57}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{initial project presentation}{59}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{an attempted \IeC {\textquotedblleft }formal\IeC {\textquotedblright } description of the work}{61}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{an attempted description in spanish}{65}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{personal statement}{69}
\defcounter {refsection}{0}\relax 
\contentsline {part}{back matter sections}{71}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{acknowledgements}{73}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{free (libre) software acknowledgements}{75}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{sources and resources}{77}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{books i read/consulted during the process}{79}
