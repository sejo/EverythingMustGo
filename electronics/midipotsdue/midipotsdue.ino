
#include "MIDIUSB.h"
/* modified from the previous one, added MIDIUSB_write example */

int potPin0 = A0;
int potPin1 = A1;

int bit0, bit1;
int newnum, num;

void setup() {
  Serial.begin(115200);
}

void loop() {
  
  bit0 = analogRead(potPin0) > 512? 1 : 0;
  bit1 = analogRead(potPin1) > 512? 1 : 0;
  newnum = 0x31 + (bit1*2 + bit0);

  if(newnum != num){
	num = newnum;
	noteOn(0, num, 64);
        MidiUSB.flush();
	delay(50);
	noteOff(0, num, 64);
        MidiUSB.flush();
  }
  delay(100);
 }



// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

