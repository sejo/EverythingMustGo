/* based on tom igoe's */

int potPin0 = A0;
int potPin1 = A1;

int bit0, bit1;
int newnum, num;

void setup() {
  //  Set MIDI baud rate:
  Serial.begin(31250);
}

void loop() {
  
  bit0 = analogRead(potPin0) > 512? 1 : 0;
  bit1 = analogRead(potPin1) > 512? 1 : 0;
  newnum = 0x31 + (bit1*2 + bit0);

  if(newnum != num){
	num = newnum;
	noteOn(0x90, num, 0x45);
	delay(50);
	noteOn(0x90, num, 0x00);
  }
  delay(100);
 }

//  plays a MIDI note.  Doesn't check to see that
//  cmd is greater than 127, or that data values are  less than 127:
void noteOn(int cmd, int pitch, int velocity) {
  Serial.write(cmd);
  Serial.write(pitch);
  Serial.write(velocity);
}
