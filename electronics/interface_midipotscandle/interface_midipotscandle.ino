
#include "MIDIUSB.h"
/* modified from the previous one, added MIDIUSB_write example */
/* Merged with NeoPixelJewel */
#include <Adafruit_NeoPixel.h>

#define NEO_PIXEL_PIN 53
#define N_NEO_PIXELS 7

#define WIDTH 3
#define HEIGHT 5
#define SCREEN_SIZE WIDTH*HEIGHT

#define DELAY_MIN 10 
#define DELAY_MAX 35

#define GLOBAL_SCALE_MIN 50
#define GLOBAL_SCALE_MAX 100
#define GLOBAL_SCALE_JUMP 2 


Adafruit_NeoPixel jewel = Adafruit_NeoPixel(N_NEO_PIXELS, NEO_PIXEL_PIN, NEO_GRBW + NEO_KHZ800);

// Stores the color for each "pixel" of the screen
unsigned long screen[SCREEN_SIZE];

// Stores the color for each NeoPixel
unsigned long neopixels[N_NEO_PIXELS];


// COLOR FUNCTIONS
byte getW(unsigned long c);
byte getR(unsigned long c);
byte getG(unsigned long c);
byte getB(unsigned long c);
unsigned long getWRGB(byte w, byte r, byte g, byte b);

unsigned long addColors(unsigned long c1, unsigned long c2);
unsigned long scaleColor(unsigned long c, int fraction);


// SCREEN FUNCTIONS
void resetScreen();
void updateNeoPixelsArray();
void updateScreen();
int getI(int x, int y);

void rect(int x, int y, int w, int h);

int globalScale;

int x,y,w,h;
unsigned long color;
long fx, fy;

int delayAmount;

// MIDI STUFF
int potPin0 = A0;
int potPin1 = A1;

int bit0, bit1;
int newnum, num;

void setup() {
  Serial.begin(115200);

  // CANDLE
	// Set gloal intensity
	globalScale = GLOBAL_SCALE_MIN;

	delayAmount = DELAY_MIN;

	jewel.begin();


	// Setup "rectangle"
	x = 0;
	y = 0;
	w = 2;
	h = 2;

  fx = 0;
  fy = 0;

	// Initial color
	color = getWRGB(60,255,40,0);
}

void loop() {
  
  bit0 = analogRead(potPin0) > 512? 1 : 0;
  bit1 = analogRead(potPin1) > 512? 1 : 0;
  newnum = 0x31 + (bit1*2 + bit0);

  if(newnum != num){
	num = newnum;
	noteOn(0, num, 64);
        MidiUSB.flush();
	delay(50);
	noteOff(0, num, 64);
        MidiUSB.flush();
  }
//  delay(100);


	// Clear screen pixels
	resetScreen();
	// "Draw" the rectangle
	rect(x,y,w,h);
	// Update the Jewel
	updateScreen();
 
	x += random(3) - 1;
	y += random(3) - 1;

	// Keep x and y within drawable borders
	x = constrain(x, 0, WIDTH - w);
	y = constrain(y, 0, HEIGHT - h);


	// Random walk the global intensity
	globalScale += random(GLOBAL_SCALE_JUMP*2)-GLOBAL_SCALE_JUMP;
	globalScale = constrain(globalScale,GLOBAL_SCALE_MIN,GLOBAL_SCALE_MAX);


	// Random walk the delay
	delayAmount += random(3)-1;
	delayAmount = constrain(delayAmount,DELAY_MIN,DELAY_MAX);
	delay(delayAmount);
 }



// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}




// COLOR FUNCTIONS
byte getW(unsigned long c){
	return (byte)(c>>24);
}

byte getR(unsigned long c){
	return (byte)(c>>16);
}

byte getG(unsigned long c){
	return (byte)(c>>8);
}

byte getB(unsigned long c){
	return (byte)(c);
}

unsigned long getWRGB(byte w, byte r, byte g, byte b){
	return (w<<24) | (r<<16) | (g<<8) | b;

}

unsigned long addColors(unsigned long c1, unsigned long c2){
	byte w1 = getW(c1);
	byte r1 = getR(c1);
	byte g1 = getG(c1);
	byte b1 = getB(c1);

	byte w2 = getW(c2);
	byte r2 = getR(c2);
	byte g2 = getG(c2);
	byte b2 = getB(c2);

	byte w3 = constrain(w1+w2,0,255);
	byte r3 = constrain(r1+r2,0,255);
	byte g3 = constrain(g1+g2,0,255);
	byte b3 = constrain(b1+b2,0,255);

	return getWRGB(w3,r3,g3,b3);
}

unsigned long scaleColor(unsigned long c, int fraction){
	byte w = (byte)constrain(getW(c)*fraction/100,0,255);
	byte r = (byte)constrain(getR(c)*fraction/100,0,255);
	byte g = (byte)constrain(getG(c)*fraction/100,0,255);
	byte b = (byte)constrain(getB(c)*fraction/100,0,255);

	return getWRGB(w,r,g,b);

}


// SCREEN FUNCTIONS
// Turn all the pixels off
void resetScreen(){
	for(int i=0; i<SCREEN_SIZE; i++){
		screen[i] = 0;
	}
}

// Set the NeoPixels array according to the Screen
void updateNeoPixelsArray(){
	unsigned long color;
	unsigned long auxcolor;

	int neopixelsWeights[N_NEO_PIXELS];
	for(int i=0; i<N_NEO_PIXELS; i++){
		neopixels[i] = 0;
	}

	for(int i=0; i<SCREEN_SIZE; i++){
		color = screen[i];

		// Reset weights that correspond to each neopixel
		for(int i=0; i<N_NEO_PIXELS; i++){
			neopixelsWeights[i] = 0;
		}

		// Hardcoding the individual contributions of the
		// NeoPixels to the pixels
		switch(i){
			case 0:
				neopixelsWeights[1] = 50;
				neopixelsWeights[6] = 50;
			break;
			case 1:
				neopixelsWeights[1] = 100;
			break;
			case 2:
				neopixelsWeights[1] = 50;
				neopixelsWeights[2] = 50;
			break;
			case 3:
				neopixelsWeights[6] = 100;
			break;
			case 4:
				neopixelsWeights[0] = 25;
				neopixelsWeights[1] = 25;
				neopixelsWeights[2] = 25;
				neopixelsWeights[6] = 25;
			break;
			case 5:
				neopixelsWeights[2] = 100;
			break;
			case 6:
				neopixelsWeights[0] = 33;
				neopixelsWeights[5] = 33;
				neopixelsWeights[6] = 33;
			break;
			case 7:
				neopixelsWeights[0] = 100;
			break;
			case 8:
				neopixelsWeights[0] = 33;
				neopixelsWeights[2] = 33;
				neopixelsWeights[3] = 33;
			break;
			case 9:
				neopixelsWeights[5] = 100;
			break;
			case 10:
				neopixelsWeights[0] = 25;
				neopixelsWeights[3] = 25;
				neopixelsWeights[4] = 25;
				neopixelsWeights[5] = 25;
			break;
			case 11:
				neopixelsWeights[3] = 100;
			break;
			case 12:
				neopixelsWeights[4] = 50;
				neopixelsWeights[5] = 50;
			break;
			case 13:
				neopixelsWeights[4] = 100;
			break;
			case 14:
				neopixelsWeights[3] = 50;
				neopixelsWeights[4] = 50;
			break;
		}

		// Scale and add the color to each neopixel
		for(int i=0; i<N_NEO_PIXELS; i++){
			auxcolor = scaleColor(color,neopixelsWeights[i]);
			auxcolor = scaleColor(auxcolor,globalScale);
			neopixels[i] = addColors(neopixels[i],auxcolor);
		}
	}


}

// Send the screen information to the Jewel
void updateScreen(){
	updateNeoPixelsArray();
	for(int i=0; i<N_NEO_PIXELS; i++){
		jewel.setPixelColor(i,neopixels[i]);
	}
	jewel.show();
}

int getIndexForCoord(int x, int y){
	return y*WIDTH + x;
}


int getI(int x, int y){
	return (int)(y*WIDTH + x);
}


void rect(int x, int y, int w, int h){
	for(int j=y; j<y+h; j++){
		for(int i=x; i<x+w; i++){
			screen[getI(i,j)] = color;
		}
	}

}

