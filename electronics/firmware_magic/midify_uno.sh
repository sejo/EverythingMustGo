#!/bin/bash
# script that re-flashes the arduino firmware to an arduino uno rev3
prev=$(pwd)
echo "going to arduino firmware directory..."
cd ~/.arduino15/packages/arduino/hardware/avr/1.6.17/firmwares/atmegaxxu2/arduino-usbserial
echo "make sure you have reset the chip (press enter to continue)"
read a
echo "erasing..."
sudo dfu-programmer atmega16u2 erase
echo "flashing hex file from HIDUINO..."
sudo dfu-programmer atmega16u2 flash Arduino-midi.hex
echo "resetting..."
sudo dfu-programmer atmega16u2 reset
echo "ready, unplug and replug the arduino"
echo "finished, returning to original folder"
cd $prev

