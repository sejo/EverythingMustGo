// generate sheets of cards for the given text.txt

size(3450,2400);


int ncols = 4;
int nrows = 2;
textSize(72);
String[] lines = loadStrings("text.txt");
color col = color(0);
float marginp = 0.1;

int n = lines.length;
int ncards = ncols*nrows;
int nsheets = n/ncards + ((n%ncards!=0)? 1 : 0);
float w = width/ncols;
float h = height/nrows;
int ptr = 0;
float x,y, tx, ty, tw, th;
String line;

// for every sheet...
for( int s=0; s<nsheets; s++){
	println("generating sheet "+s+"...");
	background(255);

	// for every card...
	for( int c=0; c<ncards; c++){
		x = (c%ncols)*w;
		y = (c/ncols)*h;

		// draw rect
		noFill();
		stroke(col);
		rect(x,y,w,h);
		
		// draw text if it still exists
		if(ptr<n){
			line = lines[ptr++];
			tx = marginp*w + x;
			ty = marginp*h + y;
			tw = w*(1-marginp*2);
			th = w*(1-marginp);
			fill(col);
			noStroke();
			text(line, tx, ty, tw, th);
		}
	} // finish card
	save(String.format("card-%02d.png",s));
} // finish sheets

//exec("convert.sh");
println("done!");
exit();
