#!/bin/bash

# clean
echo "removing any previous pdfs..."
rm *pdf

# convert to pdf
echo "converting to pdf..."
for f in *.png; do convert -verbose $f -quality 100 -units PixelsPerInch -density 300x300 ${f%png}pdf; done

# join pdfs
echo "joining pdfs..."
pdftk *pdf cat output sheets.pdf
